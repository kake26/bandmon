program bandmon;

{$mode objfpc}{$H+}

uses fphttpclient,opensslsockets,sysutils,strutils;

type

  vnstat = record

  dom: String;
  cm: String;
  vnhourly: String;
  vnmonthly: String;

end;

var 

  webres: string;
  vnstats: vnstat;
  parse: array of string;
  i: integer;

begin
try
  webres := TFPCustomHTTPClient.SimpleGet('https://192.168.1.113/bnd.dat');
  Writeln('Response : ',webres);
  parse := splitstring(webres,' ');
except
begin
// Well damn it might just be a timeout in which case we could poke it again
  Writeln('It seems we had an error proably a timeout.');
end;
end;

// Good so far now
vnstats.dom := parse[7];
vnstats.cm := parse[9];
vnstats.vnhourly := parse[11];
vnstats.vnmonthly := parse[14];

writeln('Debug Parse');

for i:=Low(parse) to high(parse) do  
    Writeln('Parse ' , i , ' ', parse[i]);  

//Writeln('Debug Parse', parse[ 0 .. length(parse)]);
Writeln('Debug ', vnstats.dom);
Writeln('Debug ', vnstats.cm);
Writeln('Debug ', vnstats.vnhourly);
Writeln('Debug ', vnstats.vnmonthly);

end.


