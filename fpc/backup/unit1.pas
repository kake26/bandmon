unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  fphttpclient, opensslsockets, strutils;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Timer1: TTimer;
    TrayIcon1: TTrayIcon;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
  private

  public

  end;

type

  vnstat = record

  dom: String;
  cm: String;
  vnhourly: String;
  vnmonthly: String;

end;

var
  Form1: TForm1;
  webres: string;
  vnstats: vnstat;
  parse: array of string;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
     Timer1.Enabled:= true;
     Button1.Caption:= 'Loading';
end;

procedure TForm1.Label7Click(Sender: TObject);
begin

end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  try
  webres := TFPCustomHTTPClient.SimpleGet('http://192.168.1.168/band/bnd.dat');
  parse := splitstring(webres,' ');

  vnstats.dom := parse[4];
  vnstats.cm := concat(parse[6],' GiB');
  vnstats.vnhourly := concat(parse[8],' ',parse[9]);
  vnstats.vnmonthly := concat(parse[11],' ', parse[12]);
  Label2.Caption := vnstats.vnhourly;
  Label6.Caption := vnstats.vnmonthly;
  Label4.Caption := vnstats.cm;
  //Label7.Caption:= parse[12] ;
 // Label8.Caption:= parse[15];
  Button1.Caption := 'Hide';
  except
    // Blah
  end;

end;

procedure TForm1.TrayIcon1Click(Sender: TObject);
begin
  Form1.Show;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Form1.Hide;
end;



end.

