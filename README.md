# bandmon
Bandwidth Monitor for OpenWrt with Desktop app

# Status: Inactive

This is phased out in favor of a Ntfy based approach for this.

# Requirements

1. A router with OpenWrt
  1.1 Must have vnstat installed
  1.2 Must have bash installed
  1.3 Must be setup to log to a remote syslog, in my case I use a Raspberry Pi, but could be anything
  
2. A Raspberry Pi - Could be any other linux system, but I have a Raspberry Pi so I'll use it
  2.1 Must have syslog-ng installed
    2.1.1 Must be setup to recieve remote log data. I use the default 514 port over UDP
  2.2 Must have bash installed
  2.3 Must have a web server that can be accessed by local network, this is how the desktop componet pulls the data
  
3. A system that can run the deskop componet which displays the data usage. Since the desktop componet is written in pure Free Pascal using lazarus it should be 
very easy to build a binary on Windows, Linux or Mac OS. Just install lazarus, open project and compile. 

# Notes

Both bash scripts are best executed once per hour via cron. I run the scripts in staggered fashion, but you can pick what works best for you. The pascal code has
the IP of my Raspberry Pi hard coded, you will need to make a quick edit to that prior to building the executable. The second bash script might need the location
where it writes bnd.dat to changed. In my case its my nginx root directory. Also vnstat and bash are not installed by default on OpenWrt so you will want to use
the web interface to install them. 
