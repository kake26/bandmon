#!/bin/bash

# step two of my revised band width monitoring system
# First script runs on openwrt, queries vnstat, crunches a number or two
# and then sends it to syslog, which then sends it to a remote server
# said remote server a RPI runs this code to make the result web accessible

# note log contains a weeks worth of hourly bandwidth logs and we need the most recent
# only

vndat=`cat /var/log/openwrt.log | grep "root: dom" | tail -n 1`; # Depending on your setup your log file name might be different

echo  "$vndat" > /var/www/html/bnd.dat; # your web root may differ