#!/bin/bash
# Requires vnstat to be running on target system

# initial values
mtotal=1000 # monthly total in GB for high end cut off
dmax=40 # daily max usage value
md=`date +%d` # day of the month numerically
cmd=$((${dmax#0} * ${md#0})) # computed max daily also the cause of my mystery should be fixed now
wif="wan"

# now vnstat stuff, some of this is specific to openwrt

vnhourly=`vnstat -i $wif -d -s --oneline | awk -F\; '{print $6}'` # total monthly usage
vnmonthly=`vnstat -i $wif -d -s --oneline | awk -F\; '{print $11}'` # daily total usage


logger "dom $md cm $cmd vnhourly $vnhourly vnmonthly $vnmonthly";
curl -d "dom $md cm $cmd vnhourly $vnhourly vnmonthly $vnmonthly" 192.168.1.168:83/bandmon